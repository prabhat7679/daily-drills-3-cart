const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 
// Q1. Find all the items with price more than $65.
function itemsMoreThan65 (){
    const itemsName = Object.values(products).filter((item)=>{
  //   console.log(item)
        const keys = Object.keys(item);
        keys.filter((item1)=>{
           // console.log(typeof products[0][item1].price)
            if(products[0][item1].price === undefined)
            {
                const product2 = products[0][item1].filter((item2)=>{
                   // console.log(item2)
                    const keys2 = Object.keys(item2);
                    return (item2[keys2[0]].price > '$65')
                    

                })
                return product2;
            }
        })
                
    });            
}
itemsMoreThan65()